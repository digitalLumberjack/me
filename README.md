digitallumberjack.me
=================================

![forest-banner](/uploads/6132ab03ee1f733626bcdc9310417360/forest-banner.gif)


This repository contains the sources of [https://digitallumberjack.me](https://digitallumberjack.me)

It uses [HUGO](https://gohugo.io/) as blog engine, and [hugo-geo](https://github.com/alexurquhart/hugo-geo) as theme.

The compilation is done by gitlab ci, and static pages are deployed on gitlab pages.

## How it works

### Build

First I write a new blog post, that is a simple markdown file (see [Markdown on Wikipedia](https://en.wikipedia.org/wiki/Markdown)).

Next I commit and push this changes onto a feature branch on gitlab. The ci build the hugo website using the Dockerfile.

Once I validate the blogpost, I merge the Merge Request from gitlab, and the ci deploys the blog on gitlab-pages : [https://digitallumberjack.me](https://digitallumberjack.me)

### DNS + HTTPS

The domain `digitallumberjack.me` has been purchased from [gandi.net](gandi.net). As they provide a free certificate for the domain, which is valid for a year, I chose to use that option in place of [let's encrypt](https://letsencrypt.org/) certificates (that I love when I can use auto renew features).

Gitlab pages can be configured to serve the website in `HTTPS`. 

- For that you have to configure your repository pages with your private key and your certificate chain.
- Once Gitlab knows the certificate, you can create a CNAME (for subdomains) or A (for domain) entry in your domain provider DNS configuration.
- Now the dns awser to `digitallumberjack.me` with the public ip of Gitlab pages server : `52.167.214.135`

## Licence

The source code is licenced MIT, the blog post are licencied CC By 4.0 [https://creativecommons.org/licenses/by/4.0/](https://creativecommons.org/licenses/by/4.0/)
