+++
tags = []
draft = false
scripts = []
css = []
highlight = true
title = "About digitalLumberjack"
description = ""
date = "2017-04-30T22:24:17+02:00"

+++

![digitalLumberjack](/images/about/digi-48.png) I'm a 32 yo french developer. I'm a lover of clean code, open source, D.I.Y., retrogaming, good beers, TV series, more recent video games, good whiskey, classic music (including classic rock and classic rap) and everything that people make with their hearth. I like to sing, dance, pretend.

![recalbox](/images/about/recalbox-48.png) I'm the proud maker of [recalboxOS](https://www.recalbox.com) ([github.com/recalbox](https://github.com/recalbox)), an operating system for many ARM based microcomputers, and for x86 and x86_64 personal computers, turning you device in a 40 in 1 video game platform.

Don't hesitate to check my [Github](https://github.com/digitalLumberjack) and [Gitlab](https://gitlab.com/digitallumberjack) accounts to see my projects.