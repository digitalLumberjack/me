+++
highlight = true
date = "2017-04-16T12:56:13+02:00"
title = "Gitlab CI and Pages"
description = ""
tags = ["blog"]
draft = false
scripts = []
css = []

+++

[Gitlab CI](https://docs.gitlab.com/ce/ci/) and [Gitlab Pages](https://docs.gitlab.com/ce/user/project/pages/) allow you to deploy automatically your static assets on a Gitlab server.

This is how this blog runs :)

So each project you create on Gitlab can have its own pages, deployed depending rules you set in the Continuous Integration file, the `.gitlab-ci.yml`.

#### What you can do with Gitlab pages and gitlab-ci:
<!--more-->

* build and test your application on each commit
* assure that every merge on master won't break the build
* deploy a static website on a custom fqdn when building a branch

#### What you cannot do:
* review apps, a feature that allow you to deploy feature branches for review, are not supported yet for gitlab pages. If you want to review your static website, you will need to get your own server (I recommend 3$/month scaleway.com servers). But we will only see here what you can do without a credit card.

#### Let's go
Create a new gitlab repository. Make it private or public, as you want.

Next you need to have some static content. I chose [H.U.G.O.](https://gohugo.io) but it can be anything, even static html files.

So let's imagine you have committed a `index.html` at the root of your gitlab project, and you want to publish that.  
```html
<html>
  <body>
    <h1>Hello Pages World!</h1>
  </body>
</html>
```

You would only have to add the `.gitlab-ci.yml` file, describing a `pages` job that contain an `artifact` targeting the `public` directory.
```yaml
pages:
  stage: deploy
  only:
  - master
  script:
  - echo deploying pages
  - mkdir public
  - cp index.html public/
  artifacts:
    paths:
    - public
```

Gitlab will detect the name of the job, `pages`, and will automatically deploy the content of the `public`directory.

If you go to your pipeline, you will see that a stage appeared:

![gitlab-pipeline](/images/gitlab/pipelines.png)

And if you go to *Settings* -> *Pages* in your repository, you will see a message saying that your pages are deployed :

![gitlab-congrats](/images/gitlab/congrats.png)

Just follow the link!

Next step : You can easily configure your pages to be on you custom fqdn. See the [Gitlab Pages Documentation](https://docs.gitlab.com/ce/user/project/pages/)
