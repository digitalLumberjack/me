+++
date = "2017-04-16T11:14:43+02:00"
tags = ["scotchshelf","diy"]
draft = false
scripts = []
css = []
highlight = true
title = "My D.I.Y. project: scotch-shelf"
description = ""

+++
![/images/shelf.jpg](/images/shelf.jpg)

The first project I want to share with you is a D.I.Y. wood shelf that contains RGB leds, controlled with either a microcontroller like [ESP8266](https://en.wikipedia.org/wiki/ESP8266) [FR](https://www.fais-le-toi-meme.fr/fr/electronique/materiel/esp8266-arduino-wifi-2-euros) or a [Raspberry Pi Zero W](https://www.raspberrypi.org/blog/raspberry-pi-zero-w-joins-family/) from a web control center.

Many good Scotch bottles will stand proudly on this creation!

The control center:

* [ ] runs on raspberry pi
* [ ] responsive
* [ ] reactive 
* [ ] allows me to add an iot item on demand

The shelf: 

* [ ] can be powered ON and OFF with an interuptor.
* [ ] when ON, the microcontroller make http requests to the server to control led colors/intensity.
* [ ] must be autonomous, and the battery can be charged with micro usb.
* [ ] one piece of wood
* [ ] diffusing plexiglass to light up the bottles


**What I want to achieve with this project:**

* [ ] learn to work with raw materials (wood)
* [ ] learn to share a D.I.Y project with the world (HUGO)
* [ ] learn to use arduino compatible microcontrollers
* [ ] learn to use [Eclipse Vert.x](http://vertx.io/) as reactive application server
* [ ] improve my skills on Angular


I bought some tools that will be usefull to work the wood, and i'll start with the control center, waiting impatiently for the packages.