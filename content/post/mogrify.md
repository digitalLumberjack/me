+++
description = ""
tags = ["tools","linux"]
draft = false
date = "2017-05-01T16:49:17+02:00"
scripts = []
css = []
highlight = true
title = "Image resize / editing"

+++

[![Imagemagick-logo](/images/mogrify/Imagemagick-logo.png)](/post/mogrify/)
[![Imagemagick-logo-2](/images/mogrify/Imagemagick-logo-2.png)](/post/mogrify/)
[![Imagemagick-logo-3](/images/mogrify/Imagemagick-logo-3.png)](/post/mogrify/)

Let's imagine, for example, that you own a blog and you sometimes write a blog post.

You may need to resize your images or create images thumbnails in batch. 

[ImageMagick](https://www.imagemagick.org) is a program/lib that can create, edit, compose, or convert bitmap images. It provides many usefull scripts that can help you.

[Mogrify](https://www.imagemagick.org/script/mogrify.php) is one of them. It provide an easy way to  processing your images.

#### Let's make some thumbsnails:

Once you have all your images in your directory create a `thumbs` subdirectory:
```bash
> ll
total 8,4M
-rw-rw-r-- 1 matthieu matthieu 4,1M avril 30 16:14 led-off.jpg
-rw-rw-r-- 1 matthieu matthieu 4,0M avril 30 16:14 led-on.jpg
-rw-rw-r-- 1 matthieu matthieu 443K avril 30 16:14 ledstrip.jpg
```
<!--more-->
```bash
> mkdir thumbs
```

And then create the thumbs of all jpg images:
```bash
> mogrify  -format jpg -auto-orient -thumbnail 200x90 -unsharp 0x.5  -path thumbs '*.jpg'
```
```bash
> ll thumbs 
total 40K
-rw-rw-r-- 1 matthieu matthieu 9,3K avril 30 16:20 led-off.jpg
-rw-rw-r-- 1 matthieu matthieu 8,2K avril 30 16:20 led-on.jpg
-rw-rw-r-- 1 matthieu matthieu  15K avril 30 16:20 ledstrip.jpg
```

#### My images are too big for a web usage
Let's resize them to a standard size, of course keeping the original ratio:
```bash
> mogrify -resize 1280x1024 *.jpg
> ll
total 1,1M
-rw-rw-r-- 1 matthieu matthieu 414K avril 30 16:24 led-off.jpg
-rw-rw-r-- 1 matthieu matthieu 379K avril 30 16:24 led-on.jpg
-rw-rw-r-- 1 matthieu matthieu 267K avril 30 16:24 ledstrip.jpg
```

Of course this feature is nothing beside the all things you can do with ImageMagick and its tools.

Check the documentation of [ImageMagick CLI](https://www.imagemagick.org/script/command-line-processing.php) and all the [ImageMagick Tools](https://www.imagemagick/org/script/command-line-tools.php)