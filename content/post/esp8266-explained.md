+++
highlight = false
title = "IOT devices inception"
description = ""
date = "2017-04-25T08:45:30+02:00"
tags = []
draft = false
scripts = []
css = []

+++

[![inception](/images/esp8266-explained/inception.jpg)](/post/esp8266-explained/)


Let's talk about **IOT**.

In [this post](/post/scotchshelf-1/) I said I wanted to learn how to use Arduino compatible micro-controllers.

Do you remember the dream layers of the movie Inception? **What I discovered is almost as disturbing**. There are a ton of available **chips**, and each chip can be integrated in a ton of **module boards**, and those module boards can be integrated on other **development modules**...

So to make it clear in my mind, I proceeded by choosing the chip first, then the module, and finally I found what I wanted with a development board.

And that is my feedback.

<!--more-->

### The chip
The chip is the core of the micro-controller. It brings the cpu, memory, connectivity features and things like cryptographic and hardware acceleration.

The chip I chose is the [ESP8266](https://en.wikipedia.org/wiki/ESP8266). I heard of it the first time at Devoxx FR 2016, by attending a conference, by [Laurent HUET](https://twitter.com/lhuet35), on the **Kit de survie pour l'IoT façon DIY**

It has the following features : 

* 32-bit RISC CPU: Tensilica Xtensa L106 running at 80 MHz*
* 64 KiB of instruction RAM, 96 KiB of data RAM
* External QSPI flash: 512 KiB to 4 MiB* (up to 16 MiB is supported)
* IEEE 802.11 b/g/n Wi-Fi
* Integrated TR switch, balun, LNA, power amplifier and matching network
* WEP or WPA/WPA2 authentication, or open networks
* 16 GPIO pins
* SPI
* I²C
* I²S interfaces with DMA (sharing pins with GPIO)
* UART on dedicated pins, plus a transmit-only UART can be enabled on GPIO2
* 10-bit ADC

That is more than what I need for this project :)

Here is the image of the ESP8266 :  
[![esp8266](/images/esp8266-explained/thumbs/esp8266.jpg)](/images/esp8266-explained/esp8266.jpg)

Price of the ESP8266 chip : ~1.50€

### The module board
The module board integrate the ESP8266 chip on a PCB that can expose some or all the feature of the chip, and bring other cool extra. 

The ESP8266 has been integrated in many module boards, and today, there are 17+ module boards as you can see on the [ESP8266 Wikipedia Page](https://en.wikipedia.org/wiki/ESP8266#AI-Thinker_modules)

The most popular module boards are the **12** series. They all have power and/or activity leds, at least 14 GPIO pins available, some extra flash memory, and are integrated in many **development board**.  
**The choice was not difficult, I decided to use an ESP8266-12-WHATEVER module board.**

You can have more information about the difference between the modules board on [this post on squix.org](https://blog.squix.org/2015/03/esp8266-module-comparison-esp-01-esp-05.html)

The ESP8266-12F module board looks like that :  
[![esp12](/images/esp8266-explained/thumbs/esp12.jpg)](/images/esp8266-explained/esp12.jpg)

And here you can see the first 11 module boards:  
[![esp](/images/esp8266-explained/thumbs/esp.jpg)](/images/esp8266-explained/esp.jpg)

Price of the ESP8266-12 board : ~4.00€

### The development board
The module board has pins for Serial communication, GPIO, flash memory, wifi antenna, etc...  
Now that I have chosen my module board, I could stop there. 

But you might want to make you development easier with a **development board**. It's a third board that integrate the module board and provide complementary features like :

* USB to serial interface
* Dupont jumpers GPIO
* Flash and reset buttons
* Support of development kits
* OOTB firmware flashing
* Direct integration with Platform.io or Arduino IDE

I bought two [NodeMcu Dev Board](https://github.com/nodemcu/nodemcu-devkit-v1.0) V1.0 and they are completely awesome! They provide all the feature listed above, and also facilitate the development and integration with the [NodeMcu Lua Firmware](https://github.com/nodemcu/nodemcu-firmware), if you want to make you programs in LUA.

[![nodemcu](/images/esp8266-explained/thumbs/nodemcu.jpg)](/images/esp8266-explained/nodemcu.jpg)

Price of the NodeMcu V1 - ESP8266-12 Dev Board : ~8.00€

------------------------------------------
So now you know how and why you should choose to buy:

* **a development board**, if this is the first esp you buy, and if you want to use it to create and test your softwares
* **module boards**, for building your devices at home, and if you already have a development board
* **chips**, what makes you a manufacturer

In the next post, we will see how you can, in 30 minutes, create, compile and start you first program with the [PlatformIO core](http://docs.platformio.org/en/latest/core.html) and the [PlatformIO IDE](http://platformio.org/)