+++
scripts = []
css = []
highlight = true
date = "2017-04-18T14:50:23+02:00"
title = "Testing HTTP Streaming responses with SpringBoot"
description = ""
tags = ["java","springboot"]
draft = false

+++

![spring boot](/images/spring-boot.png)


Yesterday I wanted to test a REST endpoint written in [SpringBoot](https://projects.spring.io/spring-boot/). The particularity of this endpoint is that it return a [Chunked Result](https://en.wikipedia.org/wiki/Chunked_transfer_encoding).

When using [SpringBoot](https://projects.spring.io/spring-boot/), Http tests are really simplified with the usage of `MockMvc`:

<!--more-->

```java
@Test
public void whenGettingTheUserServiceThenReturnTheUsers() throws Exception {
    this.mockMvc.perform(get("/v1/bi/users/10"))
            .andExpect(status().isOk())
            .andExpect(content().string(tenUsers));
}
```

This works really well with synchronous requests, but when you use asynchronous request, the http response is not received when the `andExpect` are executed, and `MockMvc` receive this response:
```
MockHttpServletResponse:
           Status = 200
    Error message = null
          Headers = {}
     Content type = null
             Body = 
    Forwarded URL = null
   Redirected URL = null
          Cookies = []
```

So you either created an endpoint returning a [DeferredResponse](http://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/web/context/request/async/DeferredResult.html) or a response that stream some data in the http stream with the help of [StreamingResponseBody](http://docs.spring.io/spring/docs/current/javadoc-api/org/springframework/web/servlet/mvc/method/annotation/StreamingResponseBody.html)

In the first case, you find many resources on the web showing you this method to await for the response in the test:
```java
@Test
public void whenGettingTheUserDeferredServiceThenReturnTheUsers() throws Exception {
    MvcResult mvcResult = mockMvc.perform(get("/v1/bi/users/deferred/10"))
            .andReturn();
    mockMvc.perform(asyncDispatch(mvcResult))
            .andExpect(status().isOk())
            .andExpect(content().string(tenUsers));
}
```

And it works like a charm.

But I struggled to find the wait to test the endpoint not only deferring the response but also streaming the data. The solution above does not work as the response is still empty.
Of course if you are streaming a huge file, the following test won't help. But in the case of dynamic streaming, this can be helpful.


You must use [MvcResult.getAsyncResult](http://docs.spring.io/spring/docs/current/javadoc-api/org/springframework/test/web/servlet/MvcResult.html#getAsyncResult--) to fetch the whole body before your assertions:

```java
@Test
public void whenGettingTheUserStreamedServiceThenReturnTheUsers() throws Exception {
    this.mockMvc.perform(get("/v1/bi/users/stream/10"))
            .andExpect(request().asyncStarted())
            .andDo(MvcResult::getAsyncResult)
            .andExpect(status().isOk())
            .andExpect(content().string(tenUsers));
}
```