+++
highlight = true
title = "iot4i first release!"
description = ""
tags = ["iot4i", "esp8266", "diy"]
draft = false
date = "2017-04-30T15:13:16+02:00"
scripts = []
css = []

+++

[![led-strip](/images/iot4i-0.1.0/ledstrip.jpg)](/post/iot4i-0.1.0/)

Last week I told you about my new project, the [scotchshelf](/post/helpme), a rgb wooden shelf.

I worked on the digital part of the project, and the [iot4i](https://gitlab.com/iot4i) project is born!

You can find two projects in the [iot4i group](https://gitlab.com/iot4i) on gitlab:

* *the [iot4i/controlcenter](https://gitlab.com/iot4i/controlcenter)* that is the web app that will help you to manage the IOT. 
* *the [iot4i/firmware](https://gitlab.com/iot4i/firmware)* that is the esp8266 microcontroller program.


The first milestone had only one **business feature** : we wanted to **set the current color from a web browser, and see that the led strip bright with this color**.

<!--more-->
One issue was open on both projects : see https://gitlab.com/iot4i/controlcenter/issues/1 and https://gitlab.com/iot4i/firmware/issues/1


## Control center
As I said earlier, I wanted to learn to use [Eclipse Vert.x](http://vertx.io/) as reactive application server. So the [iot4i/controlcenter](https://gitlab.com/iot4i/controlcenter) is a Scala [Eclipse Vert.x](http://vertx.io/) application, with a frontend in [Angular 4](https://angular.io/).

You can see the code of the release on https://gitlab.com/iot4i/controlcenter/tree/0.1.0

#### What is good so far
* **[Eclipse Vert.x](http://vertx.io/)** is really nice to use, but I need more features to implement before I can have a real opinion on it. I like the toolkit side of the library, where it is really un-opinionated, and you can do what you want, as you want. 
* **Docker/Gitlab/Gitlab-ci**: even if i work alone on the project, it's nice to use [gitlab flow](https://docs.gitlab.com/ce/workflow/gitlab_flow.html) for issues, merge requests and continuous tests/integration. Docker is extremely great for building and deploying the application.
* **Angular 4** is still really cool to play with, but a little bit hard to test efficiently for beginners.

#### What is bad so far
* [Eclipse Vert.x](http://vertx.io/) test utils and documentation still lack for Scala :( If you check the [Vert.x documentation page](http://vertx.io/docs/) you will see that the [Testing](http://vertx.io/docs/#testing) entry is the only one missing Scala, so it might be added soon.
* **Angular 4** is good, but it is a bit hard to write tests efficiently for beginners.

## Device firmware
I started to use my [NodeMcu ESP8266](/post/esp8266-explained/) and it's awesome! In less than an hour, I was already receiving data from http requests, and changing the color of the [WS2812 led strip](https://www.google.fr/search?q=ws2812btbm=vid).

The [WS2812 led strip](https://www.google.fr/search?q=ws2812btbm=vid) is controlled by a single GPIO, that send a digital signal to control the color of each led.  
With [AdaFruit NeoPixel](https://github.com/adafruit/Adafruit_NeoPixel) makes it easy to set the color of each led and send the signal to the strip.

The circuit is really simple, I just had to power up the led strip and the esp8266, and connect the data wire from a GPIO to the data of the led:

[![led-off](/images/iot4i-0.1.0/thumbs/led-off.jpg)](/images/iot4i-0.1.0/led-off.jpg)
[![led-on](/images/iot4i-0.1.0/thumbs/led-on.jpg)](/images/iot4i-0.1.0/led-on.jpg)

#### What is good so far
* **[PlatformIO Core](http://platformio.org/get-started/cli)** that bring board support, configuration, library fetching, compilation and firmware upload via serial is really efficient and easy to use.  
I was really surprised by the [Library Manager](http://docs.platformio.org/en/latest/librarymanager/) from PlatformIO, where you can describe your dependencies targeting either a standard platformio library or, more like in *Go*, a **git repository** with a specific tag, branch or commit.
* **[PlatformIO IDE](http://platformio.org/get-started)** is an IDE based on [Atom](https://atom.io/) makes it really easy to write your first program for your micro-controller. It fully integrate the PlatformIO core, and use auto-completion from clang.
* The Arduino libraries bring useful functions for GPIO usage and board specific features. 
* This is **so cool** to create and upload your first program on the microcontroller!


#### What is bad so far
* It is so easy to build and run your first firmware that you can do everything without understanding anything. I'm happy to know what is a serial communication, a toolchain, a compiler, etc... But even with that, I did not understood 50% of what happened under the hood. Be careful about that.
* The electronic is a little bit sensible with few elements: the quality of the power supply, multiple power supply, or laptop power supply that do not have a ground.
* The ESP8266 may have some incompatibilities with some libraries. A great amount of arduino libs are already compatible though.

You can see the code of the release on https://gitlab.com/iot4i/firmware/tree/0.1.0

And here is the result of this first version, in my kitchen (yes the integration in the shelf has been delayed a little bit :))

<iframe width="560" height="315" src="https://www.youtube.com/embed/KgY2w0WHhCw" frameborder="0" allowfullscreen></iframe>