+++
title = "HUGO Static website engine"
description = ""
tags = ["blog"]
draft = false
scripts = []
css = []
date = "2017-04-16T06:41:15+02:00"
highlight = true

+++

[![/images/hugo-logo-small.png](/images/hugo-logo-small.png)](https://gohugo.io/)

Back in the club with a quick little feedback on [HUGO](https://gohugo.io/) that is the static website engine that runs this blog.

I started to work on [HUGO](https://gohugo.io/) yesterday, so I can only tell you what I feel after few hours.

Let's go back few month ago when I had a try with [Jekyll](https://jekyllrb.com/) for the [recalbox.com](https://recalbox.com/blog) blog, an other static website engine written in Ruby, but I was not convinced. The thing was too low to compile, and too complex to run, even with Docker.
I made a quick homemade blog engine for recalbox.

I think [@LionelDulout](https://twitter.com/LionelDulout) talked to me about [HUGO](https://gohugo.io/) two months ago, and I told myself I would give it a try for recalbox. But I needed to make my own blog, so here it is :)

### So what do I like so far with [HUGO](https://gohugo.io/) : 
<!--more-->

#### [HUGO](https://gohugo.io/) is written in go
I must admit that I really like golang, but what I like the most are the binary of Go programs. Go compile statically your binaries and so embed all the dependencies of you program. 
Your binary will run on every system as long as your are on the same architecture than the one used at compilation.
This is how you can run go programs with only a curl + tar +exec : 
```bash
curl -L https://github.com/spf13/hugo/releases/download/v0.20.1/hugo_0.20.1_Linux-64bit.tar.gz -o hugo.tar.gz && tar xzf hugo.tar.gz && ./hugo_0.20.1_linux_amd64/hugo_0.20.1_linux_amd64
```
Check the Dockerfile of the blog sources on Gitlab, you will see how simple it is.

#### It is simple to add and edit themes
```bash
cd themes
git clone git@github.com:alexurquhart/hugo-geo.git
echo 'theme = "hugo-geo"' >> config.toml
```

#### [HUGO](https://gohugo.io/) is fast
```bash
Started building sites ...
Built site for language en:
0 of 1 draft rendered
0 future content
0 expired content
1 regular pages created
8 other pages created
0 non-page files copied
1 paginator pages created
0 tutorials created
0 tags created
total in 4 ms
```

#### [HUGO](https://gohugo.io/) supports natively i18n 
That is something I really need for recalbox... [HUGO](https://gohugo.io/) provide an i18n support that seems nice [https://gohugo.io/content/multilingual/](https://gohugo.io/content/multilingual/)

#### [HUGO](https://gohugo.io/) has a great documentation
https://gohugo.io/overview/introduction/

#### Write your blogpost in a gitlab issue
I have the possibility to use gitlab issues to write blogposts, and then just copy past the Markdown in a new HUGO .md file. That way I can write anywhere, even ask for feedback, use chromium spell check (which is not enough to write good english, I know...)

All this to say, I found the static website engine that correspond to my needs.

Checkout the sources on [https://gitlab.com/digitalLumberjack/me](https://gitlab.com/digitalLumberjack/me)